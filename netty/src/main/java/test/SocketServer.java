package test;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.http.websocketx.WebSocket13FrameDecoder;
import io.netty.handler.codec.http.websocketx.WebSocket13FrameEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

/**
 * @author 于振晗
 * @title: test.SocketServer
 * @description: TODO
 * @date 2021年11月24日
 * @path PACKAGE_NAME
 */
public class SocketServer {
    public static void main(String[] args) throws InterruptedException {
        NioEventLoopGroup masterLoop = new NioEventLoopGroup(1);
        NioEventLoopGroup workLoop = new NioEventLoopGroup();
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(masterLoop, workLoop)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(new LoggingHandler(LogLevel.INFO))
                                .addLast(new DelimiterBasedFrameDecoder(10240, Unpooled.copiedBuffer("wase".getBytes())))
                                .addLast(new WebSocket13FrameDecoder(false, true, 2048))
                                .addLast(new WebSocket13FrameEncoder(false))
                                .addLast(new ServerHandler());
                    }
                });
//                .handler(new LoggingHandler(LogLevel.INFO))
//                .handler(new StringDecoder())
//                .handler(new StringEncoder())
//                .handler(new test.ServerHandler());

        ChannelFuture sync = serverBootstrap.bind(9998).sync();
    }
}
