package test;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author 于振晗
 * @title: test.ClientHandler
 * @description: TODO
 * @date 2021年11月24日
 * @path PACKAGE_NAME
 */
public class ClientHandler extends ChannelInboundHandlerAdapter {

    public ChannelHandlerContext ctx;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        System.out.println("已连接至服务器");
        this.ctx = ctx;
        System.out.println(this.ctx);
        String str = "{ \"op\": \"subscribe\", \"args\": [ { \"channel\": \"tickers-3s\", \"instId\": \"ETH-USDT\" } ] }";
        ctx.writeAndFlush(str);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }
}
