package netty.c1;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @author 于振晗
 * @title: c1
 * @description: TODO
 * @date 2021年11月27日
 * @path netty
 */
public class TestByteBuff {
    public static void main(String[] args) {
        try (FileChannel channel = new FileInputStream("C:\\ideaFile\\java\\java8\\nettyStudy\\src\\test\\resources\\test.txt").getChannel()) {
            ByteBuffer byteBuffer = ByteBuffer.allocate(10);// 分配一个字节缓冲区,参数为缓冲区大小

            while (true) {
                int read = channel.read(byteBuffer);
                if (read == -1) { // 读完所有数据跳出循环
                    break;
                }
                byteBuffer.flip();//切换到读模式

                // 输出数据
                System.out.print("当前limit：");
                while (byteBuffer.hasRemaining()) {
                    System.out.print(byteBuffer.position() + "-" + (char) byteBuffer.get() + "||");
                }
                //切换至写模式
                byteBuffer.clear();

//                byteBuffer.compact();// 压缩缓冲区,将未读的数据前移,同时切换为写模式
                System.out.println();
            }
        } catch (IOException e) {
            System.out.println(e);
        }

    }

}
