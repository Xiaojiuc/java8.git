package netty.c1;

import java.util.UUID;

/**
 * @author 于振晗
 * @title: Test1
 * @description: TODO
 * @date 2021年11月29日
 * @path netty.c1
 */
public class Test1 {
    public static void main(String[] args) {
        System.out.println(compactSystemUuid());
    }

    public static String compactSystemUuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
