package netty.c1;

import io.netty.buffer.ByteBuf;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * @author 于振晗
 * @title: UnpackStickyBag
 * @description: TODO
 * @date 2021年12月09日
 * @path netty.c1
 */
public class UnpackStickyBag {
    public static void main(String[] args) {
        ByteBuffer source = ByteBuffer.allocate(32);
        source.put("hello world\nhelo word\nhel".getBytes(StandardCharsets.UTF_8));
        split(source);
        source.put("o world".getBytes(StandardCharsets.UTF_8));
        split(source);

    }

    static void split(ByteBuffer source){
        source.flip();
        for (int i = 0; i < source.limit(); i++) {
            //找到一条完整的信息
            if (source.get(i) == '\n') {
                // 将其放入缓冲区，计算缓冲区长度 当前坐标+1-起始坐标
                int length = i + 1 - source.position();
                ByteBuffer allocate = ByteBuffer.allocate(length);
            }
        }

        source.compact();
    }
}
