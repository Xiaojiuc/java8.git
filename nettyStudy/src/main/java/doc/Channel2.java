package doc;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;

/**
 * @author 于振晗
 * @title: Channel2
 * @description: TODO
 * @date 2021年12月09日
 * @path doc
 */
public class Channel2 extends ChannelOutboundHandlerAdapter {
    @Override
    public void flush(ChannelHandlerContext ctx) throws Exception {
        System.out.println("写出数据");
        super.flush(ctx);
    }
}
