package server;

import io.netty.buffer.ByteBuf;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

/**
 * Handles a server-side channel.
 */
public class DiscardServerHandler extends ChannelInboundHandlerAdapter { // (1)

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) { // (2)

        // 静默丢弃接收到的数据。
        /*((ByteBuf) msg).release(); // (3)*/
        //每当从客户端接收到新数据时，就会随着接收到的消息调用此方法。在本例中，接收到的消息的类型是ByteBuf。

        // 用消息做点什么，然后丢弃
        /*ByteBuf in = (ByteBuf) msg;
        try {
            while (in.isReadable()) { // (1)
                System.out.print((char) in.readByte());
                System.out.flush();
            }
        } finally {
            //要实现丢弃协议，处理程序必须忽略接收到的消息。
            //ByteBuf是一个引用计数对象，必须通过release()方法显式地释放它。
            //请记住，释放传递给处理程序的引用计数对象是处理程序的责任
            ReferenceCountUtil.release(msg); // (2)
        }*/
        //响应服务器

        ctx.writeAndFlush(msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) { // (4)
        // Close the connection when an exception is raised.
        //要实现丢弃协议，处理程序必须忽略接收到的消息。ByteBuf是一个引用计数对象，
        //必须通过release()方法显式地释放它。请记住，释放传递给处理程序的引用计数对象是处理程序的责任。
        cause.printStackTrace();
        ctx.close();
    }
}

