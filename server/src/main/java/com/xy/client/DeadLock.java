package com.xy.client;

public class DeadLock {

    public static void main(String[] args) {
        Thread t1 = new Thread(new MyDeadLock(true));
        Thread t2 = new Thread(new MyDeadLock(false));
        t1.start();
        t2.start();
    }
}

class MyDeadLock implements Runnable {
    boolean flag;
    static Object o1 = new Object();
    static Object o2 = new Object();

    public MyDeadLock(boolean flag) {
        this.flag = flag;
    }

    public void run() {
        if (this.flag) {
            System.out.println("我没发生死锁1");
            synchronized (o1) {
                System.out.println("我没发生死锁2");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                System.out.println("我没发生死锁3");
                synchronized (o2) {
                    System.out.println("我没发生死锁4");

                }
            }
        } else {
            System.out.println("我也是1");
            synchronized (o2) {
                System.out.println("我也是2");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                System.out.println("我也是3");
                synchronized (o1) {
                    System.out.println("我也是4");
                }

            }
        }
    }
}

