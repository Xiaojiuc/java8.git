package com.xy.server.util;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

/**
 * @author 于振晗
 * @title: MessageEncoder
 * @description: TODO
 * @date 2021年11月10日
 * @path com.xy.server.util
 */
public class MessageEncoder extends MessageToMessageEncoder<ByteBuf> {
    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        System.out.println("收到消息，解码");
    }
}
