package com.xy;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 于振晗
 * @title: TimeTest
 * @description: TODO
 * @date 2021年11月18日
 * @path com.xy
 */
public class TimeTest {
    public static void main(String[] args) {
        LinkedList<Pojos> pojos = new LinkedList<>();
        LocalDateTime now = LocalDateTime.now();
        for (int i = 0; i < 500; i++) {
//            System.out.println(i);
            boolean b = i % 5 == 0;
            if (b) {
                Pojos pojos1 = new Pojos("test" + 1);
                pojos.add(pojos1);
            } else {
                Pojos pojos1 = new Pojos("test" + i);
                pojos.add(pojos1);
            }
        }


        /*for (int i = 10000000; i < 20000000; i++) {
//            System.out.println(i);
            boolean b = i % 5 == 0;
            if (b) {
                Pojos pojos1 = new Pojos("test" + 1);
                pojos.add(pojos1);
            } else {
                Pojos pojos1 = new Pojos("test" + i);
                pojos.add(pojos1);
            }
        }


        for (int i = 20000000; i < 30000000; i++) {
//            System.out.println(i);
            boolean b = i % 5 == 0;
            if (b) {
                Pojos pojos1 = new Pojos("test" + 1);
                pojos.add(pojos1);
            } else {
                Pojos pojos1 = new Pojos("test" + i);
                pojos.add(pojos1);
            }
        }*/


        LocalDateTime now2 = LocalDateTime.now();
        long until = now.until(now2, ChronoUnit.MICROS);
        System.out.println("插入总数据量：" + pojos.size());
        System.out.println("插入总数据量所用时间：" + until);


        LocalDateTime now3 = LocalDateTime.now();
        LinkedList<Pojos> pojo1 = new LinkedList<>();
        // for循环
        for (Pojos pojo : pojos) {
            if (pojo.getName().equals("test1")) {
                pojo1.add(pojo);
            }
        }
        LocalDateTime now4 = LocalDateTime.now();
        long until1 = now3.until(now4, ChronoUnit.MICROS);
        System.out.println("普通for循环：" + until1);
        System.out.println(pojo1.size());

        // 并行流操作
        LocalDateTime now5 = LocalDateTime.now();

        List<Pojos> collect = pojos.stream().filter(e -> "test1".equals(e.getName())).collect(Collectors.toList());

        LocalDateTime now6 = LocalDateTime.now();
        long until2 = now5.until(now6, ChronoUnit.MICROS);
        System.out.println("stream流遍历：" + until2);
        System.out.println(collect.size());


    }
}

class Pojos {
    private String name = "xiaosan1";

    public Pojos(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
