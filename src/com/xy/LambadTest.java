package com.xy;

import java.util.*;

/**
 * @author 于振晗
 * @title: Test
 * @description: TODO
 * @date 2021年11月04日
 * @path com.xy
 */
public class LambadTest {
    public static void main(String[] args) {
        User user2 = new User(3, "123");
        User user1 = new User(2, "234");
        User user = new User(1, "345");
        List<User> list = new ArrayList<>();
        list.add(user2);
        list.add(user1);
        list.add(user);

        list.sort(Comparator.comparing(User::getName));
        System.out.println(list);
        list.sort((a, b) -> b.getName().compareTo(a.getName()));


//        Collections.sort(list);

        System.out.println(list);


    }

}



class User implements Comparable<User> {
    private Integer id;
    private String name;

    public User() {
    }

    public User(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(User user) {
        // 当前id大
//        System.out.println(this.id - user.getId());
        return user.getName().compareTo(this.name);
    }
}

