package com.xy;

import java.util.Arrays;
import java.util.List;

/**
 * @author 于振晗
 * @title: Box
 * @description: TODO
 * @date 2021年11月19日
 * @path com.xy
 */
public class Box<T> {
    private T t;

    public Box(T t) {
        this.t = t;
    }

    public T getT() {
        return t;
    }

    public <E> void print(List<E> list) {
        for (E e : list) {
            System.out.println(e.toString());
        }
    }

    public static void main(String[] args) {
        Box<String> objectBox = new Box<>("t");
        String t = objectBox.getT();
        List<String> strings = Arrays.asList("1", "2", "3", "4", "5", "6");
        objectBox.print(strings);
    }


    class Tests{
        private String id;
    }
}
