package com.xy;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author 于振晗
 * @title: GenericMethodTest
 * @description: TODO
 * @date 2021年11月19日
 * @path com.xy
 */
public class GenericMethodTest {
    public static <E> List<E> printArray(List<E> el) {
        for (E e : el) {
            System.out.println(e);
        }
        return el;
    }


    // Consumer用法
    public static <T extends String> void printString(T str, Consumer<T> consumer) {
        System.out.println("进入方法：" + str.getClass().getTypeName());
        consumer.accept(str);
        Function<T, T> function1 = x -> {
            System.out.println("123");
            return x;
        };
        System.out.println("aaa" +function1.apply(str));// 8
    }


    // Supplier用法
    public static <T> T getEmp(Supplier<T> supplier) {
        return supplier.get();
    }

    static class Emp {
        private String name;

        public String getName() {
            return name;
        }

        public Emp(String name) {
            this.name = name;
        }

//        @Override
//        public String toString() {
//            return "Emp{" +
//                    "name='" + name + '\'' +
//                    '}';
//        }
    }

    public static void main(String[] args) {
        String[] arr = {"123", "ww", "333"};
        List<String> integers = Arrays.asList(arr);
        integers.forEach(s -> {
            System.out.println(s);
        });
        System.out.println(printArray(integers));
        printString("test", o -> {
            System.out.println("当前传入的参数为" + 3);
        });
        Supplier<Emp> supplier = () -> new Emp("123");
        System.out.println(supplier.get());
        System.out.println(supplier.get());


        Function<Integer, Integer> function = integer -> 666;
        Integer apply = function.apply(123);
        System.out.println(apply);
    }
}
